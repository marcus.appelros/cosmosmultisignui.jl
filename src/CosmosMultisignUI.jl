module CosmosMultisignUI
export start

using Gtk

function start()
	box=GtkBox(:h)
	daemonlabel=GtkLabel("Daemon: ")
	daemonentry=GtkEntry()
	set_gtk_property!(daemonentry,:text,"chaind")
	msaddlabel=GtkLabel("Multisign wallet address: ")
	msaddentry=GtkEntry()
	set_gtk_property!(msaddentry,:text,"")
	reclabel=GtkLabel("Recipient: ")
	recentry=GtkEntry()
	set_gtk_property!(recentry,:text,"")
	amtlabel=GtkLabel("Amount: ")
	amtentry=GtkEntry()
	set_gtk_property!(amtentry,:text,"1stake")
	generatebtn=GtkButton("Generate tx.json")
	filelabel=GtkLabel("Tx to sign: ")
	fileentry=GtkEntry()
	set_gtk_property!(fileentry,:text,"tx.json")
	chidlabel=GtkLabel("Chain ID: ")
	chidentry=GtkEntry()
	set_gtk_property!(chidentry,:text,"chain")
	nodelabel=GtkLabel("Node: ")
	nodeentry=GtkEntry()
	set_gtk_property!(nodeentry,:text,"tcp://localhost:26657")
	signerlabel=GtkLabel("Signer: ")
	signerentry=GtkEntry()
	set_gtk_property!(signerentry,:text,"alice")
	signbtn=GtkButton("Sign")
	fileslabel=GtkLabel("Files to combine: ")
	filesentry=GtkEntry()
	set_gtk_property!(filesentry,:text,"alice-tx.json bob-tx.json")
	msnamelabel=GtkLabel("Multisign wallet name: ")
	msnameentry=GtkEntry()
	set_gtk_property!(msnameentry,:text,"alice-bob-multisig")
	combinebtn=GtkButton("Combine")
	broadcastbtn=GtkButton("Broadcast")
	starlabel1=GtkLabel("*")
	starlabel2=GtkLabel("*")
	starexpllabel=GtkLabel("*Only needed to generate the initial tx.json")
	g=GtkGrid()
	g[1,1]=daemonlabel
	g[2,1]=daemonentry
	g[1,2]=msaddlabel
	g[2,2]=msaddentry
	g[1,3]=reclabel
	g[2,3]=recentry
	g[3,3]=starlabel1
	g[1,4]=amtlabel
	g[2,4]=amtentry
	g[3,4]=starlabel2
	g[2,5]=generatebtn
	g[1,6]=filelabel
	g[2,6]=fileentry
	g[1,7]=chidlabel
	g[2,7]=chidentry
	g[1,8]=nodelabel
	g[2,8]=nodeentry
	g[1,9]=signerlabel
	g[2,9]=signerentry
	g[2,10]=signbtn
	g[1,11]=fileslabel
	g[2,11]=filesentry
	g[1,12]=msnamelabel
	g[2,12]=msnameentry
	g[2,13]=combinebtn
	g[2,14]=broadcastbtn
	g[2,15]=starexpllabel
	push!(box,g)
	win=GtkWindow(box,"Cosmos multisigner",500,600)
	showall(win)
	id = signal_connect(generatebtn, "clicked") do widget
		daemon=daemonentry.text[String]
		msadd=msaddentry.text[String]
		rec=recentry.text[String]
		amt=amtentry.text[String]
		run(pipeline(`$daemon tx bank send $msadd $rec $amt --generate-only`,stdout="tx.json"))
		println("Generated tx.json")
	end
	id = signal_connect(signbtn, "clicked") do widget
		daemon=daemonentry.text[String]
		msadd=msaddentry.text[String]
		file=fileentry.text[String]
		chid=chidentry.text[String]
		node=nodeentry.text[String]
		signer=signerentry.text[String]
		from=strip(read(`rpsd keys show $signer -a`,String))
		run(pipeline(`$daemon tx sign --from $from --multisig $msadd $file --sign-mode amino-json --chain-id $chid --node $node`,stdout="$signer-tx.json"))
		println("Generated $signer-tx.json")
	end
	id = signal_connect(combinebtn, "clicked") do widget
		daemon=daemonentry.text[String]
		msadd=msaddentry.text[String]
		file=fileentry.text[String]
		node=nodeentry.text[String]
		files=filesentry.text[String]
		msname=msnameentry.text[String]
		run(pipeline(`$daemon tx multisign --from $msname $file $msname $(split(files,' ')) --node $node`,stdout="tx_ms.json"))
		println("Generated tx_ms.json")
	end
	id = signal_connect(broadcastbtn, "clicked") do widget
		daemon=daemonentry.text[String]
		node=nodeentry.text[String]
		run(`$daemon tx broadcast tx_ms.json --node $node`)
	end
	return win
end

end # module
