# CosmosMultisignUI.jl

UI for multisign wallets in the Cosmos ecosystem

To use run the following in Julia:

```
import Pkg
Pkg.add(Pkg.PackageSpec(url="https://gitlab.com/marcus.appelros/cosmosmultisignui.jl"))
using CosmosMultisignUI
start()
```

The Gtk package is a dependency, if it isn't installed automatically you have to run `Pkg.add("Gtk")`

Run `Pkg.update("CosmosMultisignUI")` to update.

Output is shown in the terminal so keep it visible.

The daemon can be referenced by relative path, so for example if you have osmosisd in your Cosmos folder you can input `./Cosmos/osmosisd`

Only tested in Julia version 1.4.1 on Ubuntu.

The commands in the code are mainly based on the medium article by Jake Hartnell. For reference here are the commands from the article:

```
# Add keys for multisig
starsd keys add alice-ledger --pubkey <alice-pubkey-here>
starsd keys add bob-ledger --pubkey <bob-pubkey-here>
# Create multisig
starsd keys add alice-bob-multisig --multisig alice-ledger,bob-ledger --multisig-threshold 2
# Generate TX
starsd tx bank send $(starsd keys show alice-bob-multisig -a) stars123408YOURDestinationAddress 10000000ustarx --generate-only --chain-id localnet-1 > tx.json
# Alice signs
starsd tx sign --from $(starsd keys show -a alice-ledger) --multisig $(starsd keys show -a alice-bob-multisig) tx.json --sign-mode amino-json --chain-id localnet-1 >> tx-signed-alice.json
# Bob signs
starsd tx sign --from $(starsd keys show -a bob-ledger) --multisig $(starsd keys show -a alice-bob-multisig) tx.json --sign-mode amino-json --chain-id localnet-1 >> tx-signed-bob.json
# Combine signatures into single tx
starsd tx multisign --from alice-bob-multisig tx.json
alice-bob-multisig tx-signed-alice.json tx-signed-bob.json --chain-id localnet-1 > tx_ms.json
# Broadcast tx
starsd tx broadcast ms/tx_ms.json --chain-id localnet-1
```
